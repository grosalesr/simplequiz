# Simple Quiz

Project to create quizzes on different subjects.

1. Follow the example stored on quizzes directory to create your own quiz(zes)
1. Modify the config file with the name of the quiz you want to load
1. Run the application, see Execution

## Building

1. Create a virtual Environment\
    `python3 -m venv appEnv`

1. Activate the virtual environment\
    `source appEnv/bin/activate`

1. Install requirements\
    `pip install -r requirements.txt`

## Execution

1. Export the app name\
    `export FLASK_APP=main.py`

1. Run with Flask\
    `flask run`

1. Go to your web browser url\
    `localhost:5000`

## TODO

* Container version
