#!/usr/env python3
# Inspired by https://www.sitepoint.com/simple-javascript-quiz/

from classes.quiz import Quiz
from flask import Flask, request, redirect, url_for, render_template

# Creates quiz object
quiz = Quiz()
questions = quiz.shuffleQuestions()
currentQuestion = 0


app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def mainPage():
    if request.method == "GET":
        #TODO: explore flask's session variables
        global currentQuestion
        q = questions[currentQuestion]
        question = q['question']
        answers = q['answers']

        return render_template("index.html", quizSubject=quiz.subject, question=question, answers=answers)

    elif request.method == "POST":
        # HTML input label name
        questions[currentQuestion]['chosenAnswer'] = request.form.get("answers")
        currentQuestion += 1

        # internal adjustment
        quizTotalQuestions = quiz.totalQuestions() - 1
        if currentQuestion > quizTotalQuestions:
            return redirect(url_for('results'))
        else:
            return redirect(url_for('mainPage'))


@app.route('/results', methods=['GET', 'POST'])
def results():
    if request.method == "GET":
        gradedQuestions = {}

        for qNumber in range(quiz.totalQuestions()):
            q = questions[qNumber]['question']
            ca = questions[qNumber]['correctAnswer']
            cha = questions[qNumber]['chosenAnswer']

            # TODO: works but not nice though
            if quiz.grade(ca, cha):
                gradedQuestions[q] = 'lightgreen'
            else:
                gradedQuestions[q] = 'red'

        return render_template("results.html", quizSubject=quiz.subject, questions=gradedQuestions)

    elif request.method == "POST":
        # reset counter
        global currentQuestion
        currentQuestion = 0

        # see: https://en.wikipedia.org/wiki/HTTP_302
        # with 303 mandating the change of request type to GET
        return redirect(url_for('mainPage'), code=303)


if __name__ == '__main__':
    app.run()
