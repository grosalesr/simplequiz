import yaml
from random import randrange

class Quiz:
    """
    Reads config file and load the information
    """
    def __init__(self):
        self.filename = self._readConfig()
        self.data = self._loadQuiz()
        self.subject = self.data['quizSubject']
        self.questions = self.data['questions']

    def _readYAML(self, fileName:'string', isQuizFile=False):
        fileName += '.yml'
        if not isQuizFile:
            return yaml.load(open(fileName), Loader=yaml.FullLoader)
        else:
            fileName = "quizzes/" + fileName
            return yaml.load(open(fileName), Loader=yaml.FullLoader)

    def _loadQuiz(self):
        return self._readYAML(self.filename, isQuizFile=True)

    def _readConfig(self):
        # config is the default name for the app configuration
        config = "config"
        # quizFile is the keyword in the config file
        filename = "quizFile"
        return self._readYAML(config)[filename]

    def totalQuestions(self):
        return len(self.questions)

    def question(self, questionNumber:'int') -> 'dict':
        for question in range(self.totalQuestions()):
            if question == questionNumber:
                answers = []
                answersRAW = self.questions[question]['answers']
                for option in answersRAW:
                    for k, v in option.items():
                        answers.append("%s: %s" % (k, v))

                questionDetails = {
	            'question': self.questions[question]['question'],
	            'answers': answers,
                    'chosenAnswer': "",
	            'correctAnswer': self.questions[question]['correctAnswer']}
        return questionDetails

    def shuffleQuestions(self) -> 'list':
        """
        Provide some randomization for the questions
        """
        questions = []
        questionsAdded = []

        while len(questionsAdded) != self.totalQuestions():
            questionNumber = randrange(self.totalQuestions())
            if questionNumber not in questionsAdded:
                question = self.question(questionNumber)
                questionsAdded.append(questionNumber)
                questions.append(question)

        return questions

    def grade(self, correctAnswer:'string', chosenAnswer:'string') -> 'boolean':
        if correctAnswer == chosenAnswer:
            return True
        else:
            return False
